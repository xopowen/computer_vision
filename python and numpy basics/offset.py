import numpy as np
import matplotlib.pyplot as plt

image = np.loadtxt("img1.txt", delimiter='\n', dtype=str)
image2 = np.loadtxt("img2.txt", delimiter='\n', dtype=str)
#пришлось вручную переберать и приобразовывать.
# так как несработало  с np.loadtxt
def loadtxt_my(image):
    image = [x.split('\n') for x in image]
    for x in range(len(image)):
        image[x] = image[x][0].split(' ')
        for y in range(len(image[x])):
           if image[x][y].isdigit():
               image[x][y] = int(image[x][y])
           else:
               del (image[x][y])
    return image
def coordinates(im):
    for y in range(0,im.shape[0]):
        for x in range(0,im.shape[1]):
            if im[y,x] == 1:
                return y , x

image = np.array(loadtxt_my(image))
image2 = np.array(loadtxt_my(image2))

y, x = coordinates(image)
y2, x2 = coordinates(image2)
print("отклонение по y: ", y-y2,'по x: ',x-x2)

plt.figure()
plt.imshow(image, cmap='gray')
plt.show()
plt.figure()
plt.imshow(image2, cmap='gray')
plt.show()