import cv2
import numpy as np
from skimage import color
import colorsys

def det_color(hsv)->str:
    rezult  = colorsys.rgb_to_hsv(*hsv)[2]
    if rezult < 30:
        return "red"
    elif rezult < 90:
        return "yellow"
    elif rezult < 150:
        return "green"
    elif rezult < 210:
        return 'light blue'
    elif rezult < 270:
        return 'blue'
    elif rezult < 330:
        return 'pink'
    else:
        return "red"
    return  "вот тут проблемка"


def update_figure_and_color(figure, i):
    coord_pixel = contours[i][0][0]
    color_pixel = image[coord_pixel[::-1][0], coord_pixel[::-1][1]]
    color_pixel = det_color(color_pixel)
    if str(color_pixel) in figure_and_color[figure].keys():
        figure_and_color[figure][str(color_pixel)] += 1
    else:
        figure_and_color[figure].update({str(color_pixel): 1})

image = cv2.imread('balls_and_rects.png')


gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
ret, binary = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)

contours, hierarchy = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

figure_and_color = {
                    "box":{},
                    "circle":{}
                    }

for i in range(len(contours)-1):
    if len(contours[i]) == 4:
        update_figure_and_color('box',i)

    else:
        update_figure_and_color('circle', i)



print(figure_and_color)

#cv2.drawContours(image,contours,-1,(0,0,255),3)

# cv2.imshow("image",image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()