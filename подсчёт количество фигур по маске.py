import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import morphology as morp

struct_plus = np.array([[0,0,1,0,0],
                       [0,0,1,0,0],
                       [1,1,1,1,1],
                       [0,0,1,0,0],
                       [0,0,1,0,0]])

struct_krest = np.array([[1,0,0,0,1],
                         [0,1,0,1,0],
                         [0,0,1,0,0],
                         [0,1,0,1,0],
                         [1,0,0,0,1]])
stars = np.load("stars.npy").astype('uint')
result = morp.binary_hit_or_miss(stars,struct_plus)
krestiks = morp.binary_hit_or_miss(stars,struct_krest)
res = 0
for x in krestiks:
    for y in x:
        if y == True:
            res += 1
print(res)
#plt.suplot(121)
plt.imshow(stars)
plt.show()