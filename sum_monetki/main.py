import math

import matplotlib.pyplot as plt
import numpy as np

def negate(B):
    array = B.copy()
    array[np.where(array == 1)] = -1
    return array

def check(B, y, x):
    if not 0 <= x < B.shape[0]:
        return False
    if not 0 <= y < B.shape[1]:
        return False
    if B[y, x] != 0:
        return True
    return False

def neighbors2(B, y, x):
    left = y, x-1
    top = y - 1, x
    if not check(B, *left):
        left = None
    if not check(B, *top):
        top = None
    return left, top

def exists(neighbors):
    return not all([n is None for n in neighbors])

def find(label, linked):
    j = label
    while linked[j] != 0:
        j = linked[j]
    return j

def union(label1, label2, linked):

    j = find(label1, linked)
    k = find(label2, linked)
    if j != k:
        linked[k] = j


def two_pass_labeling(B):
    linked = np.zeros(len(B), dtype="uint")
    labels = np.zeros_like(B)
    label = 1
    for row in range(B.shape[0]):
        for col in range(B.shape[1]):
            if B[row, col] != 0:
                n = neighbors2(B, row, col)
                if not exists(n):
                    m = label
                    label += 1
                else:
                    lbs = [labels[i] for i in n if i is not None]

                    m = min(lbs)
                labels[row, col] = m
                for i in n:
                    if i is not None:
                        lb = labels[i]
                        if lb != m:
                            union(m, lb, linked)
    test_labels = set(labels.ravel())
    for row in range(B.shape[0]):
        for col in range(B.shape[1]):
            if B[row, col] != 0:
                new_label = find(labels[row, col], linked)
                if new_label != labels[row, col]:
                    labels[row, col] = new_label
    return marking(labels)

def marking(labels):
    re_labels = list(set(labels.ravel()))
    for row in range(labels.shape[0]):
        for col in range(labels.shape[1]):
            if B[row, col] != 0:
                labels[row, col] = re_labels.index(labels[row, col])
    return labels


def neighbors(x, y):
    return (y, x + 1), (y + 1, x), (y, x - 1), (y - 1, x)

def distance(px1,px2):
    return((px1[0] - px2[0]) ** 2 + (px1[1] - px2[1]) ** 2) ** 0.5

def centroid(LB,label = 1):
    pxs = np.where(LB == label)
    cy = np.mean(pxs[0])
    cx = np.mean(pxs[1])
    return cy, cx

def get_boundaries(LB,label = 1):
    pxs = np.where(LB == label)
    boundaries = []
    for x, y in zip(*pxs):
        for yn, xn, in neighbors(y,x):
            if yn < 0 or yn > LB.shape[0] - 1:
                boundaries.append((y,x))
                break
            elif xn < 0 or xn > LB.shape[1] - 1:
                boundaries.append((y, x))
                break
            elif LB[yn, xn]!= label:
                boundaries.append(( y, x))
                break
    return boundaries


def radil_distance(LB,label = 1):
    r,c = centroid(LB, label)
    boundaries = get_boundaries(LB, label)

    K = len(boundaries)
    rd = 0
    for rk, ck in boundaries:
        rd += distance((r, c),(rk, ck))
    return int(rd / K)

def std_radial(LB,label = 1):
    r, c = centroid(LB, label)
    rd = radil_distance(LB,label)
    boundaries = get_boundaries (LB, label)
    K = len(boundaries)
    sr = 0
    for rk, ck in boundaries:
        sr += (distance((r, c), (rk, ck)) - rd) ** 2

    return (sr/K) ** 0.5

def circularity_std(LB, label = 1):
    return radil_distance(LB,label) / std_radial(LB,label)


B = np.load("ps.npy").astype('uint')

LB = two_pass_labeling(B)

list_LB = list(set(LB.ravel()))[1:]
print(len(list_LB))
set_LB = {}
for i in list_LB:
    i = str(len(get_boundaries(LB, i)))

    if i in set_LB.keys():
        set_LB.update({i: set_LB[i]+1})
    else:
       set_LB.update({i:1})


print(set_LB)

#ошибка гдето в функции radil_distance
#хз где
#решил в обход

plt.imshow(LB)
plt.show()

