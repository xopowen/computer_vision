from functools import reduce

import cv2
import numpy as np
import random

cam = cv2.VideoCapture(0)

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("ROI", cv2.WINDOW_KEEPRATIO)
# green - 72 175 172  blue = 97 255 152  жолтый = 30 220 200
#red = 8 215 235
lower_green = (50, 100, 100)
upper_green = (72, 255, 255)

lower_yolk = (25, 100, 100)
upper_yolk = (35, 255, 255)

lower_blue = (100, 100, 100)
upper_blue = (210, 255, 255)

lower_pink = (140,150,150)
upper_pink = (190,255,255)


def find_balls(image,lower_now,upper_now):
    blurred = cv2.GaussianBlur(image, (11, 11), 0)

    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv, lower_now, upper_now)

    mask = cv2.erode(mask, None, iterations = 2)

    mask = cv2.dilate(mask, None, iterations = 2)

    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

    return cnts, mask


def has_color(item,index, list_x, list_y, shape: tuple) -> bool:  # shape(x,y)
    index_here = 0
    for x in range(0, shape[0] + 1, 2):
        for y in range(0, shape[1] + 1, 2):
            #print(item,index,index_here)
            if item in (set(list_x[x: x + 2]) & set(list_y[y: y + 2])) and index_here == index:
                return True
            index_here += 1;
    return False

list_find_color = {}
random_color_list =[]

list_color = {
    "green": (lower_green, upper_green),
    "yolk": (lower_yolk, upper_yolk),
    "blue": (lower_blue, upper_blue),
    "red": (lower_pink, upper_pink)
}

shape = (2,2)
sum_element = 0

while cam.isOpened():

    #if len(shape) != 2:
    #    shape = (int(input("введите размерность x: ")),int(input("введите размерность y: ")))
    #    sum_element = shape[0]*shape[1]
    _, image = cam.read()


    if random_color_list == []:
        random_color_list = list_color.keys()
        random_color_list = random.sample(random_color_list, k=len(random_color_list))
        print(random_color_list)

    for i in list_color.keys():
        cnts ,mask= find_balls(image, list_color[i][0], list_color[i][1])
        if len(cnts) > 0:
            c = max(cnts, key=cv2.contourArea)
            (curr_x, cuur_y), radius = cv2.minEnclosingCircle(c)
            if radius > 10:
                list_find_color.update({i : (curr_x,cuur_y) })
                cv2.circle(image, (int(curr_x), int(cuur_y)), int(radius), list_color[i][1], 2)
                cv2.circle(image, (int(curr_x), int(cuur_y)), 2,list_color[i][1], 2)
                cv2.imshow('mask', mask)
        else:
            list_find_color = {}


    list_find_color_key = list_find_color.keys()
    # if len(list_find_color_key) == 4:
    #     list_find_color2 = dict(sorted(list_find_color.items(), key=lambda x: x[1]))
    #     end =list( filter(lambda x:x[0]==x[1] ,zip(list_find_color2.keys(), random_color_list)))
    #     if len(end) == 3:
    #         cv2.putText(image, 'GOOD JOP', (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 0))
    #     list_find_color = {}

    if len(list_find_color_key) == 4:
        sort_x = list(dict(sorted(list_find_color.items(), key=lambda x: x[1][0])).keys())
        sort_y = list(dict(sorted(list_find_color.items(), key=lambda x: x[1][1])).keys())
        #print(sort_x,sort_y)

        for i in range(len(random_color_list)):
            if has_color(random_color_list[i],i,sort_x,sort_y,shape) == False:
                break
            else:
                cv2.putText(image, "GOOD", (100,160), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255))

#тут


    cv2.imshow('Camera', image)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
cam.release()
cv2.destroyAllWindows()
"""
        if random_color_list[0] in (set(sort_x[:2]) & set(sort_y[:2])):
            #print(set(sort_x[:2]) & set(sort_y[:2]))
            cv2.putText(image, str(random_color_list[0]), (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255))

        if random_color_list[1] in (set(sort_x[:2]) & set(sort_y[2:])):
           # print(set(sort_x[:2]) & set(sort_y[3:]))
            cv2.putText(image, str(random_color_list[1]), (10, 260), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255))

        if random_color_list[2] in (set(sort_x[2:]) & set(sort_y[:2])):
           # print(set(sort_x[3:]) & set(sort_y[:2]))
            cv2.putText(image, str(random_color_list[2]), (260, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255))

        if random_color_list[3] in (set(sort_x[2:]) & set(sort_y[2:])):
            #print(set(sort_x[3:]) & set(sort_y[3:]))
            cv2.putText(image, str(random_color_list[3]), (260, 260), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255))



        if random_color_list[0] in (set(sort_x[:2]) & set(sort_y[:2])):
            #print(set(sort_x[:2]) & set(sort_y[:2]))
            cv2.putText(image, str(random_color_list[0]), (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255))

        if random_color_list[1] in (set(sort_x[:2]) & set(sort_y[2:])):
           # print(set(sort_x[:2]) & set(sort_y[3:]))
            cv2.putText(image, str(random_color_list[1]), (10, 260), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255))

        if random_color_list[2] in (set(sort_x[2:]) & set(sort_y[:2])):
           # print(set(sort_x[3:]) & set(sort_y[:2]))
            cv2.putText(image, str(random_color_list[2]), (260, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255))
        print(set(sort_x[2:]) , set(sort_y[2:]))
        if random_color_list[3] in (set(sort_x[2:]) & set(sort_y[2:])):
            #print(set(sort_x[3:]) & set(sort_y[3:]))
            cv2.putText(image, str(random_color_list[3]), (260, 260), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (255, 255, 255))

"""

       # if list(sorted(list_find_color.items(), key=lambda x: x[1][0]))[0] == list_find_color_key[0]:
       #     cv2.putText(image, list_find_color_key[0], (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 0))

        #if list(sorted(list_find_color.items(), key=lambda x: x[1][0]))[3] == list_find_color_key[3]:
        #    cv2.putText(image, list_find_color_key[3], (10, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 0))
