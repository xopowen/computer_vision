from functools import reduce


import cv2
import numpy as np
import random

cam = cv2.VideoCapture(0)

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
cv2.namedWindow("ROI", cv2.WINDOW_KEEPRATIO)
while cam.isOpened():
    _, image = cam.read()

    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray,(3,3),0)
    cnts = cv2.Canny(gray,100,200)

    ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    cv2.drawContours(image, contours, -1, (0, 255, 0), 3)
    key = cv2.waitKey(1)
    if key == ord('q'):
        break
    cv2.imshow('Camera', cnts)
    cv2.imshow('Camera2', image)
cam.release()
cv2.destroyAllWindows()
