import cv2
import numpy as np
from multiprocessing import Process, Queue,Pool,Manager,set_start_method
import sys,os

cam = cv2.VideoCapture(0)

cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)
cv2.createTrackbar('h1', 'Camera', 0, 255, lambda x:x)
text = ''


def show_text_in_page(t_This,f_This):
    sys.stdin = os.fdopen(f_This)  # open stdin in this process
    some_str = ""
    some_str = input("> ")
    if some_str != "":
         t_This.put(some_str)



if __name__ == '__main__':
    #способ запуска 2 процесса
    set_start_method('spawn')
    fn = sys.stdin.fileno()#get original file descriptor
    #передача резултьтатов работы в нужное время
    t = Queue()
    q = Queue()
    process_one = Process(target = show_text_in_page, args = (t,fn,))
    process_one.start()
    fn = ''

    arrow = ''

    while cam.isOpened():
        _, image = cam.read()

        if t.qsize() > 0:
            text = t.get()
            fn = sys.stdin.fileno()
            print(text)

        h1 = cv2.getTrackbarPos('h1', 'Camera')


        gray = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

        thresh = cv2.inRange(gray, (80,h1,h1), (255,255,255))
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours =  sorted(contours,key=lambda x:len(x),reverse=True)
        if len(contours) != 0:
            arrow = contours[0]

        moments = cv2.moments(arrow)
        centroid = (int(moments['m10'] / moments['m00']),
                    int(moments['m01'] / moments['m00']))


        hull = cv2.convexHull(arrow)
        for i in range(1, len(hull)):
            cv2.line(image, tuple(*hull[i - 1]), tuple(*hull[i]), (0, 255, 0), 2)



        cv2.circle(image, centroid, 4, (0, 255, 0), 4)
        text_centroid = (centroid[0]-50*3,centroid[1])
        cv2.putText(image,text ,text_centroid,cv2.FONT_HERSHEY_SIMPLEX, 3, (0, 255, 0),4,
                    cv2.LINE_4)


        key = cv2.waitKey(1)
        if key == ord('q'):
            break
        cv2.imshow('Camera', image)
    cam.release()
    cv2.destroyAllWindows()
