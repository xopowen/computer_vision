import numpy as np
import matplotlib.pyplot as plt
from skimage.measure import label,regionprops
from skimage import filters
from scipy import ndimage
from skimage.filters import threshold_otsu
import math


def sort_maks(e):
    return e.area

def open_file(file):
    image = plt.imread('images/img ('+str(file)+').jpg')
    image = np.mean(image,2)
    sobel = filters.sobel(image)

    threshold = filters.threshold_li(sobel)
    sobel[sobel<threshold] = 0
    sobel[sobel>0] =1

    return sobel

def word_filters(sobel):
#заполняем пропуски
    sobel = ndimage.binary_fill_holes(sobel)

    #убираем лишнее до оптимального уровня
    for i in range(5):

        sobel = ndimage.binary_erosion(sobel)


    return sobel

def get_list_regions(sobel):

    labeled  = label(sobel)
    print()
    regions = regionprops(labeled)

    regions = sorted(regions,key = sort_maks,reverse = True)
    return regions

    #получение осей элипса
def get_ratio_main_os(reg):
    y0, x0 = reg.local_centroid
    orientation = reg.orientation
    x1 = x0 + math.cos(orientation) * 0.5 * reg.minor_axis_length
    y1 = y0 - math.sin(orientation) * 0.5 * reg.minor_axis_length
    x2 = x0 - math.sin(orientation) * 0.5 * reg.major_axis_length
    y2 = y0 - math.cos(orientation) * 0.5 * reg.major_axis_length
    return  math.sqrt((x1-x0)**2+(y1-y0)**2),math.sqrt((x2-x0)**2+(y2-y0)**2)

def recognize_pencil(one_os,two_os):
    if one_os / two_os > 15 or two_os /one_os > 15:
        return True
    return False

pencil = 0
for file_nember in range(1,13):
    sobel = open_file(file_nember)
    sobel_filder = word_filters(sobel)
    regions = get_list_regions(sobel_filder)

    for region in regions:

        if region.area < 8000:
            continue


        one_os,two_os = get_ratio_main_os(region)

        if recognize_pencil(one_os,two_os):
            pencil += 1


print(pencil)
"""
x0, y0 = get_ratio_main_os(regions[6])
print(x0,y0)
print(x0, y0)
plt.subplot(121)
plt.imshow(image)
plt.subplot(122)
plt.imshow(regions[6].image)
#plt.plot(y0, x0, '.g', markersize=15)
plt.show()
"""