import numpy as np
import matplotlib.pyplot as plt

def lerp(v0, v1, t):
    return (1 - t) * v0 + t * v1

size = 100
image = np.zeros((size, size, 3), dtype="uint8")
assert image.shape[0] == image.shape[1]

color1 = [0, 0, 0]
color2 = [255, 255, 255]

for i, v in enumerate(np.linspace(0, 1, image.shape[0])):
    for z,v2 in enumerate(np.linspace(v,1,image.shape[0])):
        image[i][z] = [lerp(color1[0],color2[0],v2),
                       lerp(color1[1], color2[1], v2),
                       lerp(color1[2], color2[2], v2)
                       ]
#print (image.reshape(100,100,3))
plt.figure(1)
plt.imshow(image)
plt.show()